package ru.t1.didyk.taskmanager.api.service;

import ru.t1.didyk.taskmanager.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
